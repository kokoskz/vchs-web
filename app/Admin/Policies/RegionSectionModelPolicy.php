<?php


namespace App\Admin\Policies;

use App\Models\Region;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class RegionSectionModelPolicy
{
    use HandlesAuthorization;

    /**
     * @param User   $user
     * @param string $ability
     * @param Region $item
     *
     * @return bool
     */
    public function before(User $user, $ability, $item)
    {
        if ($user->isSuperAdmin()) {
            return true;
        }

        return false;
    }

    /**
     * @param User $user
     * @param Region $item
     *
     * @return bool
     */
    public function display(User $user, Region $item)
    {
        return true;
    }

    /**
     * @param User $user
     * @param Region $item
     *
     * @return bool
     */
    public function create(User $user, Region $item)
    {
        return true;
    }

    /**
     * @param User $user
     * @param Region $item
     *
     * @return bool
     */
    public function edit(User $user, Region $item)
    {
        return true;
    }

    /**
     * @param User $user
     * @param Region $item
     *
     * @return bool
     */
    public function delete(User $user, Region $item)
    {
        return true;
    }

    /**
     * @param User $user
     * @param Region $item
     *
     * @return bool
     */
    public function restore(User $user, Region $item)
    {
        return true;
    }
}