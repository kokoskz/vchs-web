<?php

namespace App\Admin\Http\Sections;

use AdminColumn;
use AdminColumnFilter;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\SaveAndCreate;
use SleepingOwl\Admin\Section;

/**
 * Class District
 *
 * @property \App\Models\District $model
 *
 * @see https://sleepingowladmin.ru/#/ru/model_configuration_section
 */
class District extends Section implements Initializable
{
    /**
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title = "Районы";

    /**
     * @var string
     */
    protected $alias;

    /**
     * Initialize class.
     */
    public function initialize()
    {
        $this->addToNavigation()->setPriority(100)->setIcon('fa fa-city');
    }

    /**
     * @param array $payload
     *
     * @return DisplayInterface
     */
    public function onDisplay($payload = [])
    {
        $columns = [
            AdminColumn::text('id', '#')->setWidth('50px')->setHtmlAttribute('class', 'text-center'),
            AdminColumn::link('name', 'Наименование', 'created_at')
                ->setSearchCallback(function($column, $query, $search){
                    return $query
                        ->orWhere('name', 'like', '%'.$search.'%')
                    ;
                }),
            AdminColumn::custom('Область', function(Model $model) {
                $region = \App\Models\Region::query()->where('id',$model->region_id)->first();
                return $region['name'];
            })->setWidth('150px')
        ];

        $display = AdminDisplay::datatables()
            ->setName('firstdatatables')
            ->setOrder([[0, 'asc']])
            ->setDisplaySearch(true)
            ->paginate(25)
            ->setColumns($columns)
            ->setHtmlAttribute('class', 'table-primary table-hover th-center')
        ;

        if (!\Auth::user()->isSuperAdmin()) {
            $display->getScopes()->push(['Regions', \Auth::user()->region_id]);
        }

        if (\Auth::user()->isSuperAdmin()) {
            $display->setColumnFilters([
                AdminColumnFilter::select()
                    ->setModelForOptions(\App\Models\Region::class, 'id')
                    ->setLoadOptionsQueryPreparer(function ($element, $query) {
                        return $query;
                    })
                    ->setDisplay('name')
                    ->setColumnName('region_id')
                    ->setPlaceholder('Область')
                ,
            ]);

            $display->getColumnFilters()->setPlacement('card.heading');
        }

        return $display;
    }

    /**
     * @param int|null $id
     * @param array $payload
     *
     * @return FormInterface
     */
    public function onEdit($id = null, $payload = [])
    {
        $form = AdminForm::panel();

        $elementsTab1 = [
            AdminFormElement::text('name', 'Наименование (рус)')
                ->required(),
            AdminFormElement::text('name_kk', 'Наименование (каз)'),
        ];

        if (\Auth::user()->isSuperAdmin()) {
            $elementsTab1[] = AdminFormElement::select('region_id', 'Область')->setModelForOptions(\App\Models\Region::class,'name');
        } else  {
            $elementsTab1[] = AdminFormElement::hidden('region_id')->setDefaultValue(\Auth::user()->region_id);
        }

        $elementsTab1[] = AdminFormElement::wysiwyg('description', 'Паспорт (рус)');
        $elementsTab1[] = AdminFormElement::wysiwyg('description_kk', 'Паспорт (каз)');

        $tabs = AdminDisplay::tabbed([
            'Район' => new \SleepingOwl\Admin\Form\FormElements([
                AdminFormElement::columns()->addColumn($elementsTab1),
            ]),
            'Контакты акиматов сельских округов' => new \SleepingOwl\Admin\Form\FormElements([
                AdminFormElement::hasMany('subContacts',[
                    AdminFormElement::text('name', 'Наименование')->required(),
                    AdminFormElement::text('phone', 'Телефон')->required(),
                    AdminFormElement::select('lang','Локализация')
                        ->setEnum(['ru','kk'])->setDefaultValue('ru'),
                ]),
            ]),
        ]);

        $form->addElement($tabs);

        $form->getButtons()->setButtons([
            'save'  => new SaveAndClose(),
            'cancel'  => (new Cancel()),
        ]);

        return $form;
    }

    /**
     * @return FormInterface
     */
    public function onCreate($payload = [])
    {
        return $this->onEdit(null, $payload);
    }

    /**
     * @return bool
     */
    public function isDeletable(Model $model)
    {
        return true;
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}
