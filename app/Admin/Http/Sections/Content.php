<?php

namespace App\Admin\Http\Sections;

use AdminColumn;
use AdminColumnFilter;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use App\Models\District;
use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\SaveAndCreate;
use SleepingOwl\Admin\Section;

/**
 * Class Content
 *
 * @property \App\Models\Content $model
 *
 * @see https://sleepingowladmin.ru/#/ru/model_configuration_section
 */
class Content extends Section implements Initializable
{
    /**
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = "Статьи";

    /**
     * @var string
     */
    protected $alias;

    /**
     * Initialize class.
     */
    public function initialize()
    {
        $this->addToNavigation()->setPriority(100)->setIcon('fas fa-newspaper');
    }

    /**
     * @param array $payload
     *
     * @return DisplayInterface
     */
    public function onDisplay($payload = [])
    {
        $columns = [
            AdminColumn::text('id', '#')->setWidth('50px')->setHtmlAttribute('class', 'text-center'),
            AdminColumn::link('name', 'Заголовок'),
            AdminColumn::text('contentSection.name', 'Категория'),
            AdminColumn::custom('Область', function(Model $model) {
                $region = \App\Models\Region::query()->where('id',$model->region_id)->first();
                if ($region == null) {
                    return  "не выбрана";
                }
                return $region['name'];
            })->setWidth('150px'),
        ];

        $display = AdminDisplay::datatables()
            ->setName('firstdatatables')
            ->setOrder([[0, 'asc']])
            ->paginate(25)
            ->setColumns($columns)
            ->setHtmlAttribute('class', 'table-primary table-hover th-center')
        ;

        return $display;
    }

    /**
     * @param int|null $id
     * @param array $payload
     *
     * @return FormInterface
     */
    public function onEdit($id = null, $payload = [])
    {

        $array = [
            AdminFormElement::text('name', 'Заголовок')->required(),
            AdminFormElement::select('contentSectionsId', 'Категория')
                ->setModelForOptions(\App\Models\ContentSection::class,'name')
                ->required(),
        ];
        if (\Auth::user()->isSuperAdmin()) {
            $array[] = AdminFormElement::select('region_id', 'Область')
                ->setModelForOptions(\App\Models\Region::class,'name');

        } else  {
            $array[] = AdminFormElement::hidden('region_id')->setDefaultValue(\Auth::user()->region_id);
        }

        $array[] = AdminFormElement::text('video', 'Ссылка на YouTube');
        $array[] = AdminFormElement::images('images', 'Галерея');
        $array[] = AdminFormElement::files('files', 'Файлы');
        $array[] = AdminFormElement::wysiwyg('body');
        $array[] = AdminFormElement::select('lang','Локализация')
            ->setEnum(['ru','kk'])->setDefaultValue('ru');

        $columns = AdminFormElement::columns([]);

        $columns->addColumn($array,  'col-xs-12 col-sm-6 col-md-4 col-lg-4');

        $columns->setHtmlAttribute('enctype', 'multipart/form-data');

        $form = AdminForm::card()->addBody($columns);

        $form->getButtons()->setButtons([
            'save'  => new SaveAndClose(),
            'cancel'  => (new Cancel()),
        ]);


        return $form;
    }

    /**
     * @return FormInterface
     */
    public function onCreate($payload = [])
    {
        return $this->onEdit(null, $payload);
    }

    /**
     * @return bool
     */
    public function isDeletable(Model $model)
    {
        return true;
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}
