<?php

namespace App\Admin\Http\Sections;

use AdminColumn;
use AdminColumnFilter;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use App\Models\District;
use App\Models\Leader;
use App\Models\SourceFinancing;
use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\SaveAndCreate;
use SleepingOwl\Admin\Section;

/**
 * Class Organization
 *
 * @property \App\Models\Organization $model
 *
 * @see https://sleepingowladmin.ru/#/ru/model_configuration_section
 */
class Organization extends Section implements Initializable
{
    /**
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Организации';

    /**
     * @var string
     */
    protected $alias;

    /**
     * Initialize class.
     */
    public function initialize()
    {
        $this->addToNavigation()->setPriority(100)->setIcon('fa fa-building');
    }

    /**
     * @param array $payload
     *
     * @return DisplayInterface
     */
    public function onDisplay($payload = [])
    {
        $columns = [
            AdminColumn::text('id', '#')->setWidth('50px')->setHtmlAttribute('class', 'text-center'),
            AdminColumn::link('name', 'Полное название',)
                ->setSearchCallback(function($column, $query, $search){
                    return $query
                        ->orWhere('name', 'like', '%'.$search.'%')
                    ;
                }),
            AdminColumn::text('short_name', 'Принятое сокращение'),
            AdminColumn::text('email', 'E-mail'),
            AdminColumn::text('address', 'Адрес'),
            AdminColumn::text('phone', 'Телефон'),
            AdminColumn::text('lang', 'Локализация')->setWidth('140px'),
        ];



        $display = AdminDisplay::datatables()
            ->setName('firstdatatables')
            ->setOrder([[0, 'asc']])
            ->setDisplaySearch(true)
            ->paginate(25)
            ->setColumns($columns)
            ->setHtmlAttribute('class', 'table-primary table-hover th-center')
        ;

        $display->getColumnFilters()->setPlacement('card.heading');

        return $display;
    }

    /**
     * @param int|null $id
     * @param array $payload
     *
     * @return FormInterface
     */
    public function onEdit($id = null, $payload = [])
    {

        $form = AdminForm::panel();

        $tabs = AdminDisplay::tabbed([
            'Организация' => new \SleepingOwl\Admin\Form\FormElements([
                AdminFormElement::columns()->addColumn([
                    AdminFormElement::text('name', 'Полное название')->required(),
                    AdminFormElement::text('short_name', 'Принятое сокращение')->required(),
                    AdminFormElement::text('email', 'E-mail')->required(),
                    AdminFormElement::text('address', 'Адрес')->required(),
                    AdminFormElement::text('phone', 'Телефон')->required(),
                    AdminFormElement::select('districtId', 'Территория деятельности ')
                        ->setModelForOptions(District::class,'name')
                        ->required(),
                    AdminFormElement::text('subject_activity', 'Цель/предмет деятельности, основные направления деятельности'),
                    AdminFormElement::text('target_groups', 'Целевые группы'),
                    AdminFormElement::multiselect('sourceFinancings', 'Источники финансирования', SourceFinancing::class)->setDisplay('name'),
                    AdminFormElement::select('lang','Локализация')
                        ->setEnum(['ru','kk'])->setDefaultValue('ru'),
                ]),
            ]),
            'Руководство' => new \SleepingOwl\Admin\Form\FormElements([
                AdminFormElement::hasMany('leaders',[
                    AdminFormElement::text('name', 'ФИО')->required(),
                    AdminFormElement::text('phone', 'Телефон')->required(),
                ]),
            ]),
        ]);

        $form->addElement($tabs);

        $form->getButtons()->setButtons([
            'save'  => new SaveAndClose(),
            'cancel'  => (new Cancel()),
        ]);

        return $form;
    }

    /**
     * @return FormInterface
     */
    public function onCreate($payload = [])
    {
        return $this->onEdit(null, $payload);
    }

    /**
     * @return bool
     */
    public function isDeletable(Model $model)
    {
        return true;
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}
