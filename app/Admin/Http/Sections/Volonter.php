<?php

namespace App\Admin\Http\Sections;

use AdminColumn;
use AdminColumnFilter;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Section;
use App\Models\District;

/**
 * Class Volonter
 *
 * @property \App\Models\Volonter $model
 *
 * @see https://sleepingowladmin.ru/#/ru/model_configuration_section
 */
class Volonter extends Section implements Initializable
{
    /**
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title = "Волонтеры";

    /**
     * @var string
     */
    protected $alias;

    /**
     * Initialize class.
     */
    public function initialize()
    {
        $this->addToNavigation()->setPriority(100)->setIcon('fa fa-users');
    }

    /**
     * @param array $payload
     *
     * @return DisplayInterface
     */
    public function onDisplay($payload = [])
    {
        $columns = [
            AdminColumn::text('id', '#')->setWidth('50px')->setHtmlAttribute('class', 'text-center'),
            AdminColumn::link('username', 'ФИО')
                ->setSearchCallback(function($column, $query, $search){
                    return $query
                        ->orWhere('name', 'like', '%'.$search.'%')
                    ;
                }),
            AdminColumn::text('gender', 'Пол')->setWidth('100px'),
            AdminColumn::text('district.name', 'Район'),
            AdminColumn::text('city', 'Город'),
            AdminColumn::text('phone', 'Телефон'),
            AdminColumn::text('identifier','Номер волонтера'),
            AdminColumn::custom('Область', function(Model $model) {
                $region = \App\Models\Region::query()->where('id',$model->region_id)->first();
                return $region['name'];
            })->setWidth('150px')
        ];

        $display = AdminDisplay::datatables()
            ->setName('firstdatatables')
            ->setOrder([[0, 'asc']])
            ->setDisplaySearch(true)
            ->paginate(25)
            ->setColumns($columns)
            ->setHtmlAttribute('class', 'table-primary table-hover th-center')
        ;

        $display->getColumnFilters()->setPlacement('card.heading');

        if (!\Auth::user()->isSuperAdmin()) {
            $display->getScopes()->push(['Regions', \Auth::user()->region_id]);
        }

        if (\Auth::user()->isSuperAdmin()) {
            $display->setColumnFilters([
                AdminColumnFilter::select()
                    ->setModelForOptions(\App\Models\Region::class, 'id')
                    ->setLoadOptionsQueryPreparer(function ($element, $query) {
                        return $query;
                    })
                    ->setDisplay('name')
                    ->setColumnName('region_id')
                    ->setPlaceholder('Область')
                ,
            ]);
        }

        return $display;
    }

    /**
     * @param int|null $id
     * @param array $payload
     *
     * @return FormInterface
     */
    public function onEdit($id = null, $payload = [])
    {
        $array = [
            AdminFormElement::text('username', 'ФИО')
                ->required(),
            AdminFormElement::select('gender','Пол')
                ->setEnum(['жен.','муж.'])
                ->required(),
            AdminFormElement::date('birthDay', 'Дата рождения')
                ->required(),
        ];

        if (\Auth::user()->isSuperAdmin()) {
            $array[] = AdminFormElement::select('region_id', 'Область')
                ->setModelForOptions(\App\Models\Region::class,'name')
                ->required();

        } else  {
            $array[] = AdminFormElement::hidden('region_id')->setDefaultValue(\Auth::user()->region_id);
        }

//        $array[] = AdminFormElement::select('districtId', 'Район')
//            ->setModelForOptions(District::class,'name')
//            ->required();

        $array[] = AdminFormElement::dependentselect('districtId', 'Район')
            ->setModelForOptions(District::class, 'name')
            ->setDataDepends(['region_id'])
            ->setLoadOptionsQueryPreparer(function($item, $query) {
                return $query->where('region_id', $item->getDependValue('region_id'));
            })->required();

        $array[] = AdminFormElement::text('city', "Город")
            ->required();
        $array[] = AdminFormElement::text('phone','Телефон')
            ->required();
        $array[] = AdminFormElement::text('identifier','Номер волонтера')
            ->required();
        $array[] = AdminFormElement::textarea('information','Дополнительная информация');
        $array[] = AdminFormElement::image('image','Изображение');

        $columns = AdminFormElement::columns([]);

        $columns->addColumn($array,  'col-xs-12 col-sm-6 col-md-4 col-lg-4');

        $columns->setHtmlAttribute('enctype', 'multipart/form-data');

        $form = AdminForm::card()->addBody($columns);

        if (\Auth::user()->isAdminRegion() || \Auth::user()->isSuperAdmin()) {
            $form->getButtons()->setButtons([
                'save_and_close' => new SaveAndClose(),
                'cancel' => (new Cancel()),
            ]);
        } else {
            $form->getButtons()->setButtons([
                'cancel' => (new Cancel()),
            ]);
        }

        return $form;
    }

    /**
     * @return FormInterface
     */
    public function onCreate($payload = [])
    {
        return $this->onEdit(null, $payload);
    }

    /**
     * @return bool
     */
    public function isDeletable(Model $model)
    {
        if (\Auth::user()->hasRole("admin")) {
            return true;
        }

        return false;
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}
