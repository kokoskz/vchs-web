<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use SleepingOwl\Admin\Contracts\Navigation\NavigationInterface;
use SleepingOwl\Admin\Providers\AdminSectionsServiceProvider as ServiceProvider;

class AdminSectionsServiceProvider extends ServiceProvider
{

    /**
     * @var array
     */
    protected $sections = [
        \App\Models\User::class => 'App\Admin\Http\Sections\Users',
        \App\Models\District::class => 'App\Admin\Http\Sections\District',
        \App\Models\Volonter::class => 'App\Admin\Http\Sections\Volonter',
        \App\Models\ContentSection::class => 'App\Admin\Http\Sections\ContentSection',
        \App\Models\Content::class => 'App\Admin\Http\Sections\Content',
        \App\Models\Organization::class => 'App\Admin\Http\Sections\Organization',
        \App\Models\Region::class => 'App\Admin\Http\Sections\Region',
    ];

    /**
     * Register sections.
     *
     * @param \SleepingOwl\Admin\Admin $admin
     * @return void
     */
    public function boot(\SleepingOwl\Admin\Admin $admin)
    {
        $this->registerPolicies( 'App\\Admin\\Policies\\' );

        $this->app->call( [ $this, 'registerRoutes' ] );
        $this->app->call( [ $this, 'registerNavigation' ] );

        parent::boot($admin);


    }

    /**
     * @param NavigationInterface $navigation
     */
    public function registerNavigation( NavigationInterface $navigation )
    {
        require_once base_path( 'app/Admin/navigation.php' );
    }

    /**
     * @param Router $router
     */
    public function registerRoutes( Router $router )
    {
        $router->group([
            'prefix'     => config( 'sleeping_owl.url_prefix' ),
            'middleware' => config( 'sleeping_owl.middleware' )
        ], function ($router) {
            require_once base_path( 'app/Admin/routes.php' );
        });
    }

}
