<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
    protected $table = 'organizations';

    public $timestamps = false;

    protected $guarded = [];

    public function contentSection()
    {
        return $this->hasOne(ContentSection::class, 'id', 'contentSectionsId');
    }

    public function leaders()
    {
        return $this->hasMany( Leader::class);
    }

    public function sourceFinancings()
    {
        return $this->belongsToMany( SourceFinancing::class);
    }

    public function scopeWithSourceFinancing($query, $sourceFinancingId)
    {
        $query->whereHas('source_financings', function ($q) use ($sourceFinancingId) {
            $q->where('source_financing_id', $sourceFinancingId);
        });
    }

    public function setSourceFinancingIdAttribute($sourceFinancingId)
    {
        $this->save();
        $leader = SourceFinancing::find($sourceFinancingId);
        $this->leaders()->attach($leader);
    }
}