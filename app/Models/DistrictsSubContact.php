<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class DistrictsSubContact extends Model
{
    protected $table = 'districts_sub_contacts';

    public $timestamps = false;

    protected $guarded = [];
}