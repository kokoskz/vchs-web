<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    protected $table = 'contents';

    public $timestamps = false;

    protected $guarded = [];

    public function contentSection()
    {
        return $this->hasOne(ContentSection::class, 'id', 'contentSectionsId');
    }

    public function getImagesAttribute($value)
    {
        return preg_split('/,/', $value, -1, PREG_SPLIT_NO_EMPTY);
    }

    public function setImagesAttribute($images)
    {
        $this->attributes['images'] = implode(',', $images);
    }

    public function scopeRegions($query, $regionId)
    {
        $query->where('region_id', $regionId);
    }
}