<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    protected $table = 'districts';

    public $timestamps = false;

    protected $guarded = [];

    public function subContacts()
    {
        return $this->hasMany( DistrictsSubContact::class);
    }

    public function scopeRegions($query, $regionId)
    {
        $query->where('region_id', $regionId);
    }
}