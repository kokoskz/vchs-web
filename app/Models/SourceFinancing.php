<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class SourceFinancing extends Model
{
    protected $table = 'source_financings';

    public $timestamps = false;

    protected $guarded = [];

    public function organizations()
    {
        return $this->belongsToMany(Organization::class, 'organization_source_financing', 'source_financing_id');
    }

    public function setOrganizationsAttribute($organizations)
    {
        $this->organizations()->detach();
        if (! $organizations) {
            return;
        }

        if (! $this->exists) {
            $this->save();
        }

        $this->organizations()->attach($organizations);
    }
}