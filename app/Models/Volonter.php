<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Volonter extends Model
{
    protected $table = 'volonters';

    public $timestamps = false;

    protected $guarded = [];

    public function district()
    {
        return $this->hasOne(District::class, 'id', 'districtId');
    }

    public function scopeRegions($query, $regionId)
    {
        $query->where('region_id', $regionId);
    }
}