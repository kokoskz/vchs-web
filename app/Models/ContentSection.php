<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class ContentSection extends Model
{
    protected $table = 'content_sections';

    public $timestamps = false;

    protected $guarded = [];
}