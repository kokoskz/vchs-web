<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Leader extends Model
{
    protected $table = 'leaders';

    public $timestamps = false;

    protected $guarded = [];

}