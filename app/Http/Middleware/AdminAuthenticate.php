<?php
namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;
class AdminAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string|null $guard
     *
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::check()) {
            $role = Auth::user()->role;
            if ($role == 'mchs' || $role == 'admin' || $role == 'admin_region') {
                return $next($request);
            }
        }

        return abort(404);
    }
}