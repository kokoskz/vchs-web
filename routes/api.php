<?php

use App\Models\Content;
use App\Models\District;
use App\Models\Organization;
use App\Models\Region;
use App\Models\Volonter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('volonters', function() {
    return Volonter::with('district')->get();
});

Route::get('regions', function() {
    return Region::query()->get();
});

Route::get('contents', function () {
    return Content::with('contentSection')->get();
});

Route::get('district', function () {
    return District::with('subContacts')->get();
});

Route::get('organization', function () {
    return Organization::with(['leaders','sourceFinancings','contentSection'])->get();
});