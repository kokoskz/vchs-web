<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRegionToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('region_id')->unsigned();
            DB::statement("ALTER TABLE users MODIFY COLUMN role ENUM('default', 'mchs', 'admin','admin_region') NOT NULL DEFAULT 'default'");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('region_id');
            DB::statement("ALTER TABLE users MODIFY COLUMN role ENUM('default', 'mchs', 'admin') NOT NULL DEFAULT 'default'");
        });
    }
}
