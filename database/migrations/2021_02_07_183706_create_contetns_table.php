<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContetnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('contentSectionsId')->unsigned();
            $table->longText('body')->nullable();
            $table->string('video')->nullable();
            $table->text('images')->nullable();
            $table->text('files')->nullable();
            $table->enum('lang', ['ru', 'kk'])->default('ru');
            $table->timestamps();
        });

        Schema::table('contents', function($table) {
            $table->foreign('contentSectionsId')->references('id')->on('content_sections')->onDelete('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contents');
    }
}
