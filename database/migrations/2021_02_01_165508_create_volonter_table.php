<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVolonterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('volonters', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username');
            $table->date('birthDay')->nullable();
            $table->integer('districtId')->unsigned();
            $table->string('city');
            $table->string('phone')->nullable();
            $table->enum('gender', ['жен.','муж.'])->nullable();
            $table->string('information')->nullable();
            $table->string('image')->nullable();
            $table->enum('lang', ['ru', 'kk'])->default('ru');
        });

        Schema::table('volonters', function($table) {
            $table->foreign('districtId')->references('id')->on('districts')->onDelete('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('volonters');
    }
}
