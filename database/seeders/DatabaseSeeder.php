<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

//        DB::table('users')->insert([
//            'name'     => 'admin',
//            'email'    => 'admin@test.com',
//            'password' => bcrypt('password'),
//            'role'     => 'admin',
//        ]);
//
//
//        $categories = [
//            [
//                "name" => "Что делать если?"
//            ],
//            [
//                "name" => "Первая помощь"
//            ],
//            [
//                "name" => "Энциклопедия"
//            ],
//            [
//                "name" => "Волонтеры"
//            ],
//            [
//                "name" => "Организации"
//            ],
//        ];
//
//        foreach ($categories as $item) {
//            DB::table('content_sections')->insert([
//                'name'     => $item,
//            ]);
//        }
//
//        $source_financing = [
//            [
//                "name" => "Государственный социальный заказ",
//                "name_kk" => "Государственный социальный заказ",
//            ],
//            [
//                "name" => "Гранты международных и казахстанский фондов",
//                "name_kk" => "Гранты международных и казахстанский фондов",
//            ],
//            [
//                "name" => "Государственный заказ",
//                "name_kk" => "Государственный заказ",
//            ],
//            [
//                "name" => "Спонсорская помощь",
//                "name_kk" => "Спонсорская помощь",
//            ],
//            [
//                "name" => "Производственно-хозяйственная деятельность",
//                "name_kk" => "Производственно-хозяйственная деятельность",
//            ],
//            [
//                "name" => "Зарубежные гранты",
//                "name_kk" => "Зарубежные гранты",
//            ],
//            [
//                "name" => "Самофинансирование",
//                "name_kk" => "Самофинансирование",
//            ],
//            [
//                "name" => "Отсутствуют",
//                "name_kk" => "Отсутствуют",
//            ],
//        ];
//
//        foreach ($source_financing as $item) {
//            DB::table('source_financings')->insert([
//                'name'     => $item['name'],
//                'name_kk'     => $item['name_kk'],
//            ]);
//        }

        DB::table('regions')->insert([
            'name'     => 'Восточно-казахстанская область',
            'name_kk'     => 'Шығыс Қазақстан облысы',
        ]);

        DB::table('regions')->insert([
            'name'     => 'Алматинская область',
            'name_kk'     => 'Алматы облысы',
        ]);

        DB::table('districts')->update(['region_id' => 1]);
        DB::table('users')->update(['region_id' => 1]);
        DB::table('volonters')->update(['region_id' => 1]);
        DB::table('volonters')->update(['region_id' => 1]);
    }
}
