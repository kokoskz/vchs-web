valet stop
brew unlink php@7.3
brew link --force --overwrite php@7.3
brew services start php@7.3
composer global update
rm -f ~/.config/valet/valet.sock
valet install